# -*- coding: utf-8 -*-
import re
refactor_keywords = re.compile(r"refactor|organiz|clean", re.IGNORECASE)
defective_keywords = re.compile(r"bug|fix|defect|error|now|work", re.IGNORECASE)
import numpy as np



def Labeler(df):
    for f, c, t in zip(df.FileName, df.RelatedPriorCommits, df.text):
        if re.search(defective_keywords, t):
            if c != 'set()':
                for h in c.split():
                    df.loc[(df.CommitHash == h)  & (df.FileName == f), 'DEFECTIVE'] = True
            else:
                continue
        else:
            continue
        
    df["REFACTOR"] = df["text"].apply(lambda x: True if re.search(refactor_keywords, x) else False)
    df = df.replace(np.nan, '')