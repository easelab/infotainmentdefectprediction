# -*- coding: utf-8 -*-

import pandas as pd
from sklearn import model_selection ,preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score,classification_report 
from sklearn.metrics import auc
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics.scorer import make_scorer
from sklearn.model_selection import GridSearchCV
import numpy as np





def LearningModel(dforiginal, learner_name, scale=False):
    df = dforiginal.copy()
    
    LR_parameter_space = {
                                   'penalty' : ['l1','l2'],
                                    'dual' : [False],
                                    'tol' : [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1],
                                    'C' : [1000, 100, 10, 1, 0.1, .01, .001],
                                    'solver' : ['liblinear', 'newton-cg','lbfgs','sag','saga'],
                                    'multi_class' : ['ovr','auto'],
                                    'fit_intercept' : [False, True],
                                    'class_weight' : ['balanced',None]
                                    }
    
    MLP_parameter_space = {
                                'hidden_layer_sizes': [(50,50,50), (50,100,50), (100,)],
                                'activation': ['tanh', 'relu','identity','logistic'],
                                'solver': ['sgd', 'adam','lbfgs'],
                                'alpha': [0.0001, 0.05],
                                'learning_rate': ['constant','adaptive'],
                                'tol' : [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1],
                                'max_iter' : [100,300,500] ,
                                'learning_rate' : ['constant','invscaling','adaptive']
                            }
    
    NB_parameter_space = {
#                          "priors" : ["None"],
#                          "var_smoothing" : [1e-9]
                          }
    
    RF_parameter_space = { 
                            'bootstrap': [True, False],
                             'n_estimators': [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000],
                             'min_samples_leaf': [1, 2, 4],
                             'min_samples_split': [2, 5, 10],
                             'max_features': ['auto', 'sqrt', 'log2'],
                             'max_depth': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None],
                             'criterion' :['gini', 'entropy']
                             }


    
    if scale is True:
        min_max_scaler = preprocessing.MinMaxScaler()   
        #returns dataframe with preprocessed columns
        df[['Distinct Developers Count','Active Developers Count',
                         'Commmit Count','Added Lines','AddedLines_count','Deleted Lines',
                         'Cyclomatic Complexity','Author Experience',
                         'Commiter Experience']] = min_max_scaler.fit_transform(df[['Distinct Developers Count','Active Developers Count',
                         'Commmit Count','Added Lines','AddedLines_count','Deleted Lines',
                         'Cyclomatic Complexity','Author Experience','Commiter Experience']])
    
    X = df.iloc[:, :-1]
    y = df.iloc[:, -1]

    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.20,random_state=101)
    
    my_scorer = make_scorer(CostEffectiveness, greater_is_better=True)
    
    
    model = ""
    parameters = ""
    if learner_name == 'LogisticRegression':
        model = LogisticRegression
        parameters = LR_parameter_space
    elif learner_name == 'MLPClassifier':
        model = MLPClassifier
        parameters = MLP_parameter_space
    elif learner_name == 'GaussianNB':
        model = GaussianNB
        parameters = NB_parameter_space
    elif learner_name == 'RandomForestClassifier':
        model = RandomForestClassifier
        parameters = RF_parameter_space
    else:
        raise ValueError('YOU HAVE NOT SPECIFIED A VALID NAME FOR YOUR LEARNING MODEL')

    gsearch = RandomizedSearchCV(model(), parameters, cv=10, scoring= my_scorer , error_score =0.0)
    gsearch.fit(X_train, y_train)
    bestmodel = gsearch.best_estimator_
    
    print(X_train)
    print(y_train)

    print("FINISHED TRAINING THE MODEL: " + learner_name)
    
    predicted_classes = bestmodel.predict(X_test)
    print(classification_report(y_test, predicted_classes))
    print("Accuracy: ", accuracy_score(y_test, predicted_classes))
    
    bestmodel.feature_names = list(X_train.columns.values)
    bestmodel.name = learner_name
    bestmodel.accuracy = accuracy_score(y_test, predicted_classes)
    bestmodel.classification_report = classification_report(y_test, predicted_classes)
 
    p = [i[1] for i in bestmodel.predict_proba(X_test)]
    return p, y_test, bestmodel


def CostEffectiveness(probs, trueClasses, plot=False):
    
#    probs_class_1 = [i[1] for i in probs]
    probs_and_trueClasses = [(p, c) for p, c in zip(probs, trueClasses)]
    
    unsorted_list = probs_and_trueClasses
    sorted_list = sorted(probs_and_trueClasses, key=lambda x: x[0], reverse=True)
    
    
    sorted_bugs_found = []
    unsorted_bugs_found = []
    for i in range(0, len(unsorted_list)-1):
        if sorted_list[i][1] == True:
            sorted_bugs_found.append(1)
        else:
            sorted_bugs_found.append(0)
            
        if unsorted_list[i][1] == True:
            unsorted_bugs_found.append(1)
        else:
            unsorted_bugs_found.append(0)

    bins = 10
    bin_width = int(round(len(sorted_bugs_found)/ bins))
    
    sorted_binned_sums = [sum(sorted_bugs_found[i:i+bin_width]) for i in range(0, len(sorted_bugs_found), bin_width)]
    sorted_binned_cumsums = np.cumsum(sorted_binned_sums)
    
    unsorted_binned_sums = [sum(unsorted_bugs_found[i:i+bin_width]) for i in range(0, len(unsorted_bugs_found), bin_width)]
    unsorted_binned_cumsums = np.cumsum(unsorted_binned_sums)

    sorted_percentages = [sorted_binned_cumsums[i]/sorted_binned_cumsums[-1]*100 for i in range(0,len(sorted_binned_cumsums))]
    unsorted_percentages = [unsorted_binned_cumsums[i]/unsorted_binned_cumsums[-1]*100 for i in range(0,len(unsorted_binned_cumsums))]


    x = np.arange(0, 110, 10)
    
    optimal = list(100*np.ones(10))
    optimal = [0] + optimal
    sorted_percentages = [0] + sorted_percentages
    unsorted_percentages = [0] + unsorted_percentages
    
    model_auc = auc(x,sorted_percentages)
    baseline_auc = auc(x,unsorted_percentages)
    optimal_auc = auc(x, optimal)

    if plot:
        
        plt.plot(x, sorted_percentages, label="model")
        plt.plot(x, unsorted_percentages, label="baseline")
        plt.plot(x, optimal, label="optimal")
        plt.legend()
        plt.suptitle('Cost Effectiveness of Model', fontsize=18)
        plt.xlabel('Percentage of Sampled Files (%)', fontsize=14)
        plt.ylabel('Percentage of Defects Found (%)', fontsize=14)
    
        plt.savefig("CE.png")
        plt.show()
        
        print('Model AUC: {:.0f}'.format(model_auc))
        print('Random Baseline AUC: {:.0f}'.format(baseline_auc))
        print('Optimal AUC: {0:.0f}'.format(optimal_auc))
        print('Model Evaluation: \n Absolute AUC Difference from Baseline : {0:.0f} \n Percenteage of AUC increase with respect to Baseline: {1:.0f}% \n AUC Ratio compared to Optimal: {2:.0f}%'.format((model_auc - baseline_auc), (model_auc/baseline_auc-1)*100, model_auc/optimal_auc*100))
  
    return model_auc

